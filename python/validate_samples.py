#!/usr/bin/env python2.7

import argparse
import os
import shutil
import sys
import time

class TermColors:

    @classmethod
    def Bold(cls, s):
        return '\033[1m' + s + '\033[0m'
    
    @classmethod
    def SuccessGreen(cls, s):
        return '\033[92m' + s + '\033[0m'
    
    @classmethod
    def Note(cls, s):
        return '\033[7;37;40m' + s + '\033[0m'
        
    @classmethod
    def Underline(cls, s):
        return '\033[4m' + s + '\033[0m'
    
    @classmethod
    def Warning(cls, s):
        return '\033[93m' + s + '\033[0m'
    
    @classmethod
    def Fail(cls, s):
        return '\033[91m' + s + '\033[0m'
    
    
def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory', required=True, help='input directory')
    parser.add_argument('--device', help='ALSA device for playback')
    parser.add_argument('--amp', type=int, default=0, help='additional gain [-60, 60]')
    return parser.parse_args()
    
def test_sample(pipeline, filename):
    pipeline.set_state(Gst.State.PLAYING)
    return raw_input('Keep {}? [y]es, [n]o, [r]eplay: '.format(filename))
        
if __name__ == '__main__':
    args = parse_arguments()
    args.directory = os.path.abspath(args.directory)
    
    if not os.path.exists(args.directory):
        print('No folder {}'.format(args.directory))
        sys.exit(1)
    
    import gi
    gi.require_version('Gst', '1.0')
    from gi.repository import GObject, Gst
    
    GObject.threads_init()
    Gst.init(None)

    nsamples = 0
    nkept    = 0
    
    rejected_folder = os.path.join(args.directory, 'rejected')
    if not os.path.exists(rejected_folder):
        print(TermColors.Warning('Created folder {} for rejected samples.'.format(rejected_folder)))
        os.makedirs(rejected_folder)
    
    print('')
    
    for w in os.listdir(args.directory):
        if w.endswith('.wav'):
            nsamples += 1
            pipeline = Gst.parse_launch('filesrc name=source ! decodebin ! audioconvert ! rgvolume pre-amp={} ! alsasink name=sink'.format(args.amp))
            source   = pipeline.get_by_name('source')
            sink     = pipeline.get_by_name('sink')
            
            pipeline.set_state(Gst.State.READY)
            if args.device:
                sink.set_property('device', args.device)
            
            full_path = os.path.join(args.directory, w)
            source.set_property('location', full_path)
            
            
            while True:
                pipeline.set_state(Gst.State.PLAYING)
                answer = raw_input('Keep {}? [y]es, [n]o, [r]eplay: '.format(TermColors.Bold(w)))
                if answer == 'r':
                    pipeline.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH, 0)
                    continue
                elif answer == 'n':
                    print TermColors.Fail('rejecting {}\n'.format(w))
                    shutil.move(full_path, os.path.join(rejected_folder, w))
                    break
                elif answer == 'y':
                    print TermColors.SuccessGreen('keeping {}\n'.format(w))
                    nkept += 1
                    break

            pipeline.set_state(Gst.State.READY)
    
    if nsamples == 0:
        print('No .wav files in {}'.format(args.directory))
        sys.exit(0)
    
    print(TermColors.Note('Done. Kept {} of {} samples from {}'.format(nkept, nsamples, args.directory)))
    
    
    
