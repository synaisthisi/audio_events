import argparse
import logging
import os
import paho.mqtt.client as mqtt
from event_recorder import EventRecorder

SETTINGS = None
ER = None
CLIENT = mqtt.Client()

def parse_arguments():
    parser = argparse.ArgumentParser(description='MQTT-controlled audio events classifier', 
        add_help=False)
    
    parser.add_argument('--help', action='help', help='Display help message and exit.')
    parser.add_argument('--host', '-h',
         required=True, help='MQTT broker IP address')
    parser.add_argument('--port', '-p', 
        type=int, default=1883, help='MQTT broker listen port' )
    parser.add_argument('--dir', '-d',   
        required=True, help='Audio events recordings root folder')
    parser.add_argument('--sub',
        required=True, help='Topic to listen for messages')
    parser.add_argument('--pub',     
        required=False, help='Topic to publish messages')
    
    return parser.parse_args()
    
def on_mqtt_connect(CLIENT, userdata, flags, rc):
    logging.info('Connected to MQTT broker')

def on_msg_start(CLIENT, userdata, message):
    global ER
    event_id = message.payload
    ER.start(event_id)
    
def on_msg_stop(CLIENT, userdata, message):
    global ER
    ER.stop()
  
if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    SETTINGS = parse_arguments()
    try:
        assert(os.path.exists(SETTINGS.dir))
    except AssertionError:
        logging.warning('Directory {} doesn\'t exist, creating a new one.'.format(SETTINGS.dir))
        os.makedirs(SETTINGS.dir)
        
    ER = EventRecorder(SETTINGS.dir)
    CLIENT.on_connect = on_mqtt_connect
    CLIENT.message_callback_add(SETTINGS.sub + '/start', on_msg_start)
    CLIENT.message_callback_add(SETTINGS.sub + '/stop', on_msg_stop)
    CLIENT.connect(SETTINGS.host, SETTINGS.port, 60)
    CLIENT.subscribe('{}/#'.format(SETTINGS.sub), 0)
    
    try:
        CLIENT.loop_forever()
    except KeyboardInterrupt:
        print('\n# Bye.')

    
