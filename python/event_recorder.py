import os
import logging
from wav_recorder import WavRecorder

class EventRecorder:
    def __init__(self, folder):
        self.recorder = WavRecorder()
        self.root_folder = folder
        self.events = {}
        for d in os.listdir(folder):
            event_folder = os.path.join(folder, d)
            if os.path.isdir(event_folder):
                self.events[d] = 0
                for w in os.listdir(event_folder):
                    if os.path.isfile(os.path.join(event_folder, w)) and w.endswith('.wav'):
                        self.events[d] += 1
        logging.info('Set recordings directory: {}'.format(folder))
        logging.info('Contents:')
        for e in self.events:
            logging.info('....{}: {}'.format(e, self.events[e]))
        
        
    def start(self, event):
        if not event in self.events:
            logging.info('New event, creating folder {}'.format(event))
            os.makedirs(os.path.join(self.root_folder, event))
            self.events[event] = 0
            
        recording_id = self.events[event] + 1
        filename = 'recording_{0:04d}.wav'.format(recording_id)
        full_path = os.path.join(*[self.root_folder, event, filename])
        self.recorder.start(full_path)
        self.events[event] += 1
        
    def stop(self):
        self.recorder.stop()
