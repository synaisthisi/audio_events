import logging
import os
import struct

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst

class WavRecorder:
    ''' A simple PCM .wav file recorder '''
        
    def __init__(self, channels=2, samplerate=44100):
        GObject.threads_init()
        Gst.init(None)
       
        pipeline_str = 'alsasrc ! audio/x-raw, channels=(int){}, rate=(int){} ! \
                audioconvert ! wavenc ! filesink name=sink'
        
        self.pipeline = Gst.parse_launch(pipeline_str.format(channels, samplerate))
        self.sink = self.pipeline.get_by_name('sink')
        
        logging.info('Created GStreamer pipeline.')
        self.pipeline.set_state(Gst.State.NULL)


    def start(self, filename):
        ''' Start recording.
        
        Note:
          Only one file can be recorded at a time. If this is called while another
          recording is in progress, it will do nothing.

        Args:
          filename (string): The name of the resulting .wav file.
        '''
        if (self.sink.get_property('location')):
            logging.warning('Already recording.')
            return

        self.sink.set_property('location', filename)
        self.pipeline.set_state(Gst.State.PLAYING)
        logging.info('Writing to {}'.format(filename))

    def stop(self):
        ''' Stop recording. '''
        current_file = self.sink.get_property('location')
        
        if not current_file:
            logging.warning('Already stopped.')
            return

        self.pipeline.set_state(Gst.State.PAUSED)
        self.pipeline.set_state(Gst.State.NULL)
        self.sink.set_property('location', None)
        size_in_bytes = os.path.getsize(current_file)
        
        logging.info('Stopped recording.')
        logging.info('Written {} bytes in {}'.format(size_in_bytes, current_file))

        if size_in_bytes > 0:
            self._fix_wav_header(current_file)

    def _fix_wav_header(self, filename):
        ''' Adds size information into .wav file's header.
        
        This must be done manually, since GStreamer doesn't write that 
        information when recording from live sources. 
        
        For information of the WAV format's header file see 
        http://www-mmsp.ece.mcgill.ca/documents/AudioFormats/WAVE/WAVE.html
        '''
        with open(filename, 'r+b') as f:
            size_in_bytes = os.path.getsize(filename)
           
            # Size of file.
            f.seek(4)
            for c in list(struct.pack('<I', size_in_bytes-8)):
                f.write(c)

            # Size of WAV's data section.
            f.seek(40)
            for c in list(struct.pack('<I', size_in_bytes-44)):
                f.write(c)
