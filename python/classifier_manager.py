import json
import logging
import os

import numpy as np

from pyAudioAnalysis import audioTrainTest as aT

class AudioEventClassifier:
    def __init__(self, info_file, classifier_name):
        try:
            with open(info_file, 'r') as f:
                global_info = json.load(f)
                self.info = global_info[classifier_name]
                try:
                    self.classifier = np.load(self.info['filename'])
                    logging.info(' Loaded {} classifier ({}) for the events: {}'.format(self.info['type'],
                        classifier_name, ', '.join([c['name'] for c in self.info['classes']])))
                except IOError:
                    logging.error('Couldn\'t load binary classifier {}'.format(self.info['filename']))
        except IOError:
            logging.error('Couldn\'t load info file {}'.format(info_file))
        except ValueError:
            logging.error('{} is not a properly formatted JSON file.'.format(info_file))
        
    @classmethod
    def from_file(cls, info_file, classifier_name):
        return cls(info_file, classifier_name)
        
    @classmethod
    def new(cls, info_file, classifier_name, method, folders):
        # Train the classifier
        
        # Update the info file
        return cls(info_file, classifier_name)
