#!/usr/bin/env python2.7

import argparse
import os
import wave

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', required=True, help='input directory.')
    parser.add_argument('-o', '--output', required=True, help='output directory.')
    parser.add_argument('-d', '--duration', type=int, default=1000, help='duration of generated files.')
    parser.add_argument('--overlap', type=float, default=0.0, help='overlap of generated files.')
    return parser.parse_args()
    
def split_wavs(in_folder, out_folder, duration=1000, overlap=0.0, name='sample{0:04d}.wav'):
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)
    
    num_generated_wavs = 0
    
    for f in os.listdir(in_folder):
        if f.endswith('.wav'):
            in_wavefile = wave.open(os.path.join(in_folder, f), 'rb')
            samplerate  = in_wavefile.getframerate()
            channels    = in_wavefile.getnchannels()
            samplewidth = in_wavefile.getsampwidth()
            
            out_frames = int((duration / 1000.0) * samplerate)
            out_bytes  = out_frames * channels * samplewidth
            overlap_frames = int(out_frames * overlap)
            
            print('Splitting {}.'.format(f))
            
            while True:
                samples = in_wavefile.readframes(out_frames)
                print in_wavefile.tell()
                if len(samples) != out_bytes:
                    break
                
                out_filename = os.path.join(out_folder, name.format(num_generated_wavs + 1))
                print out_filename
                
                out_wavefile = wave.open(out_filename, 'w')
                out_wavefile.setframerate(samplerate)
                out_wavefile.setnchannels(channels)
                out_wavefile.setsampwidth(samplewidth)
                out_wavefile.writeframes(samples)
                out_wavefile.close()
                
                print('.... {}'.format(out_filename))
                num_generated_wavs += 1
                
                in_wavefile.setpos(in_wavefile.tell() - overlap_frames)
                
            in_wavefile.close()

if __name__ == '__main__':
    args = parse_arguments()
    split_wavs(args.input, args.output, args.duration, args.overlap)
